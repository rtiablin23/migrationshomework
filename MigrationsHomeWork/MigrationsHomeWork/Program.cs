﻿using Microsoft.EntityFrameworkCore;
using MigrationsHomeWork.Models;
using System;
using System.Linq;

namespace MigrationsHomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
           using(ApplicationDbContext db = new ApplicationDbContext())
           {
                var posts = db.Posts.Include(x => x.User).ToList();
                foreach (var p in posts)
                {
                    Console.WriteLine("*****************");
                    Console.WriteLine($"User: {p.User.FirstName}  {p.User.LastName}");
                    Console.WriteLine($"Comment:{p.Comment}");
                    Console.WriteLine("*****************");

                }
            }
        }
    }
}

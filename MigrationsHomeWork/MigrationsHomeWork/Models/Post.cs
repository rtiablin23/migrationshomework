﻿using MigrationsHomeWork.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigrationsHomeWork.Models
{
    public class Post : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public string Comment { get; set; }
        public User User { get; set; }
    }
}
